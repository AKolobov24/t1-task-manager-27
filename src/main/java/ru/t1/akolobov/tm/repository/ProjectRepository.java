package ru.t1.akolobov.tm.repository;

import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
