package ru.t1.akolobov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull final AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@NotNull final String name);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull final String argument);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
