package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Project updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @Nullable String description);

}
